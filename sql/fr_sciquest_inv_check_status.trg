CREATE OR REPLACE TRIGGER fimsmgr.fr_sciquest_inv_check_status
--****************************************************************************************************************
--
--    Name
--           FR_SCIQUEST_INV_CHECK_STATUS
--   
--    Table Name
--           fimsmgr.fabinck
--   
--    Purpose
--           Track invoice check status changes for later exporting to the SciQuest HigherMarkets site.
--    
--    Modification History
--    
--   Date        Author               Description
--  -----------  -------------------  -----------------------------------------------------------------------------
--   04/24/2015  Amy Tackett          Created this trigger.
--*****************************************************************************************************************
 AFTER INSERT OR UPDATE
  ON fimsmgr.fabinck
 REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
   WHEN ( NVL( NEW.fabinck_check_type_ind, ' ' ) <> NVL( OLD.fabinck_check_type_ind, ' ' )
         OR NVL( NEW.fabinck_cancel_ind, ' ' ) <> NVL( OLD.fabinck_cancel_ind, ' ' ) )
DECLARE
  v_invoiceNumber	fabinck.fabinck_invh_code%TYPE;
  v_status			varchar2(30);
  v_paymentMethod	varchar2(30);
  v_recordNumber	fabinck.fabinck_check_num%TYPE;
  v_recordDate		varchar2(10);
  error_txt VARCHAR2(255);
  l_cnt     NUMBER;
--
PROCEDURE insert_error( error_txt IN VARCHAR )
IS
BEGIN
  INSERT INTO mu_gac.error_log_trigger VALUES ( SYSDATE, 'FR_SCIQUEST_INV_CHECK_STATUS' || ':' || error_txt );
END;
--
BEGIN
  SELECT COUNT(*)
   INTO l_cnt
  FROM fabinvh
   WHERE fabinvh_code = :NEW.fabinck_invh_code
    AND fabinvh_create_source = 'EINVOICE';
  -- If check is not tied to a SciQuest invoice do not attempt to send a status update
  IF l_cnt = 0
  THEN
    RETURN;
  END IF;
  v_invoiceNumber := :NEW.fabinck_invh_code; -- Invoice Number
  IF NVL( :NEW.fabinck_cancel_ind, 'N' ) = 'Y'
  THEN
    v_status := 'In Process'; -- Status
    v_paymentMethod := 'Cancelled'; -- Payment Method
    v_recordNumber := ' '; -- Record Number
    -- As we cannot blank out the check date from this processor we will override with a very obviously false date
    v_recordDate := '1950-01-01'; -- Record Date
  ELSE
    v_status := 'Paid'; -- Status
    IF SUBSTR( :NEW.fabinck_invh_code, 1, 1 ) = '!'
    THEN
      v_paymentMethod := 'ACH'; -- Payment Method
    ELSE
      v_paymentMethod := 'Check'; -- Payment Method
    END IF;
    v_recordNumber := :NEW.fabinck_check_num; -- Record Number
    v_recordDate := TO_CHAR( :NEW.fabinck_check_date, 'YYYY-MM-DD' ); -- Record Date
  END IF;
  INSERT INTO fzbsqex (fzbsqex_id, fzbsqex_invoice_number, fzbsqex_status, fzbsqex_payment_method, fzbsqex_record_number, fzbsqex_record_date, fzbsqex_create_timestamp)
    VALUES (fzbsqex_seq.nextval, v_invoiceNumber, v_status, v_paymentMethod, v_recordNumber, v_recordDate, sysdate); 
EXCEPTION 
  WHEN OTHERS THEN
    error_txt := SQLERRM;
    insert_error( error_txt );
END;
/
SHOW ERRORS;
