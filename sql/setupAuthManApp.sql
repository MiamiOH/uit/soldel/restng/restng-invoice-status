declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;
  
begin

  v_authman_app_name := 'Finance Web Services';
  v_authman_module_name := 'InvoiceStatus';
  v_authman_entity_name := 'FINANCE_WS_USER';
  v_authman_grantkey := 'view';
  
  -- Add Authman application. Do this only once.
  insert into a_application (application_id, name)
    values ((select nvl(max(application_id), 0) + 1 from a_application), v_authman_app_name);

  -- Add the first AuthMan module. Do this only once per module.
  insert into a_module (module_id, application_id, name) 
    values ((select nvl(max(module_id), 0) + 1 from a_module), (select application_id from a_application where name = v_authman_app_name), v_authman_module_name);

  -- Add the first AuthMan entity. Do this only once per entity.
  insert into a_entity (entity_id, dn, type, scope, application_id)
    values ((select nvl(max(entity_id), 0) + 1 from a_entity), v_authman_entity_name, 'AP', 'A', (select application_id from a_application where name = v_authman_app_name));

  -- Add the first AuthMan grantkey. Do this only once per grantkey.
  insert into a_grant_key (grant_key_id, grant_key, scope, application_id)
    values ((select nvl(max(grant_key_id), 0) + 1 from a_grant_key), v_authman_grantkey, 'A', (select application_id from a_application where name = v_authman_app_name));

  -- Add the first AuthMan authorization. Do this only once per authorization.
  insert into a_authorization (authorization_id, entity_id, module_id, grant_by, grant_date, grant_start_date, grant_end_date, grant_key_id, status)
    values ((select nvl(max(authorization_id), 0) + 1 from a_authorization), (select entity_id from a_entity where dn = v_authman_entity_name),
      (select module_id from a_module where name = v_authman_module_name and application_id = (
      (select application_id from a_application where name = v_authman_app_name))), 'doej', sysdate, sysdate, sysdate + 365, 
      (select grant_key_id from a_grant_key where grant_key = v_authman_grantkey and application_id = (select application_id from a_application where name = v_authman_app_name)), 'A');

  -- Add a new grantkey.
  v_authman_grantkey := 'delete';
  insert into a_grant_key (grant_key_id, grant_key, scope, application_id)
    values ((select nvl(max(grant_key_id), 0) + 1 from a_grant_key), v_authman_grantkey, 'A', (select application_id from a_application where name = v_authman_app_name));

  -- Add an authorization for the new grantkey using the same module and entity.
  insert into a_authorization (authorization_id, entity_id, module_id, grant_by, grant_date, grant_start_date, grant_end_date, grant_key_id, status)
    values ((select nvl(max(authorization_id), 0) + 1 from a_authorization), (select entity_id from a_entity where dn = v_authman_entity_name),
      (select module_id from a_module where name = v_authman_module_name and application_id = (
      (select application_id from a_application where name = v_authman_app_name))), 'doej', sysdate, sysdate, sysdate + 365, 
      (select grant_key_id from a_grant_key where grant_key = v_authman_grantkey and application_id = (select application_id from a_application where name = v_authman_app_name)), 'A');
      
  -- Add a new grantkey.
  v_authman_grantkey := 'update';
  insert into a_grant_key (grant_key_id, grant_key, scope, application_id)
    values ((select nvl(max(grant_key_id), 0) + 1 from a_grant_key), v_authman_grantkey, 'A', (select application_id from a_application where name = v_authman_app_name));

  -- Add an authorization for the new grantkey using the same module and entity.
  insert into a_authorization (authorization_id, entity_id, module_id, grant_by, grant_date, grant_start_date, grant_end_date, grant_key_id, status)
    values ((select nvl(max(authorization_id), 0) + 1 from a_authorization), (select entity_id from a_entity where dn = v_authman_entity_name),
      (select module_id from a_module where name = v_authman_module_name and application_id = (
      (select application_id from a_application where name = v_authman_app_name))), 'doej', sysdate, sysdate, sysdate + 365, 
      (select grant_key_id from a_grant_key where grant_key = v_authman_grantkey and application_id = (select application_id from a_application where name = v_authman_app_name)), 'A');

end;

