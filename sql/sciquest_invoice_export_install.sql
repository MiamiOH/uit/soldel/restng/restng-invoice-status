CREATE TABLE MU_GAC.ERROR_LOG_TRIGGER
(
  ERROR_DATE  DATE,
  ERROR_TEXT  VARCHAR2(4000 CHAR)
);

CREATE TABLE MU_GAC.ERROR_LOG_WEB_SERVICE
(
  ERROR_DATE      DATE,
  ERROR_TEXT      VARCHAR2(4000 CHAR),
  INVOICE_NUMBER  VARCHAR2(8),
  FZBSQEX_ID      INTEGER
);

CREATE TABLE MU_GAC.FZBSQEX
(
  fzbsqex_id				integer,
  fzbsqex_invoice_number	varchar2(8),
  fzbsqex_status			varchar2(30),
  fzbsqex_payment_method	varchar2(30),
  fzbsqex_record_number		varchar2(8),
  fzbsqex_record_date		varchar2(10),
  fzbsqex_notes				varchar2(255),
  fzbsqex_create_timestamp	timestamp
);

GRANT INSERT on MU_GAC.FZBSQEX to FIMSMGR;

CREATE OR REPLACE PUBLIC SYNONYM FZBSQEX FOR MU_GAC.FZBSQEX;

CREATE SEQUENCE MU_GAC.FZBSQEX_SEQ;
CREATE OR REPLACE PUBLIC SYNONYM FZBSQEX_SEQ FOR MU_GAC.FZBSQEX_SEQ;

GRANT SELECT on MU_GAC.FZBSQEX_SEQ to FIMSMGR;