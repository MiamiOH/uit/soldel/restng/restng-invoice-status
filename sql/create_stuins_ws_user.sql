/*****************************************************************************
  FILE NAME:  ws_authentication_add_local_users.sql
  Copyright (c) 2011 Miami University, All Rights Reserved.
  
  Miami University grants you ("Licensee") a non-exclusive, royalty free,
  license to use, modify and redistribute this software in source and
  binary code form, provided that i) this copyright notice and license
  appear on all copies of the software; and ii) Licensee does not utilize
  the software in a manner which is disparaging to Miami University.
  
  This software is provided "AS IS" and any express or implied warranties,
  including, but not limited to, the implied warranties of merchantability
  and fitness for a particular purpose are disclaimed. It has been tested
  and is believed to work as intended within Miami University's
  environment. Miami University does not warrant this software to work as
  designed in any other environment.
  
  
  AUTHOR:   Kent Covert
  
  DESCRIPTION:  Add new local users.
  
  AUDIT TRAIL:
  
  PRJ-TSK          Date        UniqueID      Version
  2111             28-APR-11   COVERTKA      1.0
  Description
    Initial creation of the script
  
  AUDIT TRAIL: END
 *****************************************************************************/
REM 
REM Comments about this template: 
REM
REM When a table is created in Banner, the following should be included in the 
REM 'create table' script:
REM 1. Connect to the database properly to ensure that the table is created
REM    within the right database instance
REM 2. Use variables for the owner, password and database instance to connect
REM    to the proper database as the appropriate table owner. 
REM 3. Drop the table
REM 4. Create the table with the storage clause at the bottom
REM 5. Grant rights to BANINST1 
REM 6. Connect as BANINST1 to create the public synomym.  Use variables for the
REM    owner, password and database instance.  
REM 7. Grant privileges to UIS_DEVELOPER_<tableowner> roles
REM
REM This template creates a table named <tablename> as <tableowner>, grants  
REM rights to BANINST1, drops and creates a unique index, then connects as
REM BANINST1 to create the public synonym. (A unique index may not be necessary  
REM for all tables.)
REM
REM To use this script as a template:
REM 1. Perform a 'replace all' on occurrences of <tableowner> with the name of 
REM    the owner, eg: SATURN
REM 2. Perform a 'replace all' on occurrences of <tablename> with the tablename,
REM    eg: SZBXXXX
REM 3. Enter the variables required for the table and remove the variables 
REm    included in the template
REM 4. Save the script as <tablename>_create_table.sql
REM
REM When the script is executed, the user will be prompted for the instance name,
REM    the password of the <tableowner> and the password of BANINST1 for the
REM    instance it is being executed in.
REM
REM **************************************************************************
REM
REM Spool the results to see what happened
REM

SPOOL ws_authentication_add_local_users.lst


REM
REM Connect to the database properly to ensure that this table is created
REM   within the right schema.
REM

/*
ACCEPT  database PROMPT 'Enter instance: ';
ACCEPT  appmgr_password PROMPT 'Enter password for appmgr in &&database: ' HIDE;
CONNECT appmgr/&&appmgr_password@&&database;
*/

REM
REM     Prompt for username and password for the account to be created
REM

ACCEPT new_password PROMPT 'Enter password for WS account: ' HIDE;
ACCEPT new_creator  PROMPT 'Enter your uniqueId: ';

SET ECHO ON

REM
REM     Insert records
REM

DECLARE
  new_username VARCHAR2(64) := 'FINANCE_WS_USER';
  new_comments VARCHAR2(256) := 'user account for student account waiver to access web services';
  new_password VARCHAR2(200) := '&new_password';
  enc VARCHAR2(200);
BEGIN

  enc := lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>new_password))));

  INSERT
    INTO appmgr.ws_authentication_local_users
         (username,
          algorithm,
          credential_hash,
          create_date,
          create_username,
          expiration_date,
          comments)
  VALUES (lower(new_username),
          '1',
          enc,
          SYSDATE,
          upper('&new_creator'),
          TO_DATE('1-JAN-2037'),
          new_comments);
END;
/

SPOOL OFF

UNDEFINE database
UNDEFINE appmgr_password
UNDEFINE new_username
UNDEFINE new_password
UNDEFINE new_comments
UNDEFINE new_creator

REM user added
