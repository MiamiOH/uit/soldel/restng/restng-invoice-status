CREATE OR REPLACE TRIGGER fimsmgr.fr_sciquest_invoice_status
--****************************************************************************************************************
--
--    Name
--           FR_SCIQUEST_INVOICE_STATUS
--   
--    Table Name
--           fimsmgr.fabinvh
--   
--    Purpose
--           Track invoice status changes for later exporting to the SciQuest HigherMarkets site.
--    
--    Modification History
--    
--   Date        Author               Description
--  -----------  -------------------  -----------------------------------------------------------------------------
--   04/24/2015  Amy Tackett  		  Created this trigger.
--*****************************************************************************************************************
 AFTER INSERT OR UPDATE
  ON fimsmgr.fabinvh 
 REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
   WHEN ( ( NVL( NEW.fabinvh_cancel_ind, ' ' ) <> NVL( OLD.fabinvh_cancel_ind, ' ' )
       OR NVL( NEW.fabinvh_appr_ind, ' ' ) <> NVL( OLD.fabinvh_appr_ind, ' ' ) )
      AND NVL( NEW.fabinvh_create_source, ' ' ) = 'EINVOICE' )
DECLARE
  v_invoiceNumber	fabinvh.fabinvh_code%TYPE;
  v_status			varchar2(30);
  error_txt 		VARCHAR2(255);
--
PROCEDURE insert_error( error_txt IN VARCHAR )
IS
BEGIN
  INSERT INTO mu_gac.error_log_trigger VALUES ( SYSDATE, 'FR_SCIQUEST_INVOICE_STATUS' || ':' || error_txt );
END;
--
BEGIN
  v_invoiceNumber := :NEW.fabinvh_code; -- Invoice Number
  IF NVL( :NEW.fabinvh_open_paid_ind, 'O' ) = 'O'
    AND NVL( :NEW.fabinvh_cancel_ind, 'N' ) = 'N'
    AND NVL( :NEW.fabinvh_appr_ind, 'N' ) = 'N'
  THEN
    v_status := 'In Process'; -- Status
  ELSIF NVL( :NEW.fabinvh_open_paid_ind, 'O' ) = 'O'
    AND NVL( :NEW.fabinvh_cancel_ind, 'N' ) = 'N'
    AND NVL( :NEW.fabinvh_appr_ind, 'N' ) = 'Y'
  THEN
    v_status := 'Payable'; -- Status
  ELSIF NVL( :NEW.fabinvh_cancel_ind, 'N' ) = 'Y'
  THEN
    v_status := 'Cancelled'; -- Status
  ELSE
    v_status := 'In Process'; -- Status
  END IF;
  INSERT INTO fzbsqex (fzbsqex_id, fzbsqex_invoice_number, fzbsqex_status, fzbsqex_create_timestamp)
    VALUES (fzbsqex_seq.nextval, v_invoiceNumber, v_status, sysdate); 
EXCEPTION 
  WHEN OTHERS THEN
    error_txt := SQLERRM;
    insert_error( error_txt );
END;
/
SHOW ERRORS;
