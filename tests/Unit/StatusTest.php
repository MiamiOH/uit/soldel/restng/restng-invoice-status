<?php

namespace MiamiOH\ProjectsInvoicestatus\Tests\Unit;

use MiamiOH\RESTng\App;

class InvoiceStatusTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $subject;

    private $querySql = '';
    private $queryId = '';
    private $queryInvoiceNumber = '';
    private $queryErrorMessage = '';

    protected function setUp()
    {

        $this->querySql = '';
        $this->queryId = '';
        $this->queryInvoiceNumber = '';
        $this->queryErrorMessage = '';

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'queryfirstcolumn', 'perform'))
            ->getMock();

        $this->dbh->error_string = '';

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->subject = new \MiamiOH\ProjectsInvoicestatus\Services\Status();
        $this->subject->setDatabase($db);

    }

    public function testGetStatusList()
    {
        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayCollectionMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array())
            ->getMock();

        $this->subject->setRequest($request);

        $resp = $this->subject->getStatusList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(2, count($payload));

        $first = $payload[0];
        $this->assertEquals(7, count(array_keys($first)), 'Assert 7 elements in the array');
        $this->assertTrue(array_key_exists('recordId', $first), 'Assert invoiceId exists');
        $this->assertTrue(array_key_exists('invoiceNumber', $first), 'Assert invoiceNumber exists');
        $this->assertTrue(array_key_exists('invoiceStatus', $first), 'Assert invoiceStatus exists');
        $this->assertTrue(array_key_exists('paymentMethod', $first), 'Assert paymentMethod exists');
        $this->assertTrue(array_key_exists('recordNumber', $first), 'Assert recordNumber exists');
        $this->assertTrue(array_key_exists('recordDate', $first), 'Assert recordDate exists');
        $this->assertTrue(array_key_exists('notes', $first), 'Assert notes exists');

    }

    public function testDeleteStatus()
    {

        $this->queryId = '2';

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithId')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn($this->queryId);

        $this->subject->setRequest($request);

        $resp = $this->subject->deleteStatus();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testLogError()
    {

        $this->queryId = '2';
        $this->queryInvoiceNumber = 'W0207511';
        $this->queryErrorMessage = 'there was an error';

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithErrorMessage')),
                $this->callback(array($this, 'performWithInvoiceNumber')),
                $this->callback(array($this, 'performWithId')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())
            ->method('getData')
            ->willReturn(array(
                'recordId' => $this->queryId,
                'invoiceNumber' => $this->queryInvoiceNumber,
                'errorMessage' => $this->queryErrorMessage));

        $this->subject->setRequest($request);

        $resp = $this->subject->logError();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }


    public function queryall_arrayWithQuery($subject)
    {
        $this->querySql = $subject;
        return true;
    }

    public function queryall_arrayCollectionMock()
    {
        $records = array(
            array(
                'record_id' => '1',
                'invoice_number' => 'W0207510',
                'invoice_status' => 'payable',
                'payment_method' => 'ACH',
                'record_number' => '1234',
                'record_date' => '2015-04-29',
                'notes' => 'test note',
            ),
            array(
                'record_id' => '2',
                'invoice_number' => 'W0207510',
                'invoice_status' => 'payable',
                'payment_method' => '',
                'record_number' => '',
                'record_date' => '',
                'notes' => '',
            ),
        );

        return $records;
    }

    public function performWithQuery($subject)
    {
        $this->querySql = $subject;
        return true;
    }

    public function performWithId($subject)
    {
        return $subject === $this->queryId;
    }

    public function performWithInvoiceNumber($subject)
    {
        return $subject === $this->queryInvoiceNumber;
    }

    public function performWithErrorMessage($subject)
    {
        return $subject === $this->queryErrorMessage;
    }

    public function performMock()
    {
        return true;
    }

}