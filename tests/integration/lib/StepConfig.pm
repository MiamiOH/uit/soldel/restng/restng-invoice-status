#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw($server $authInfo $dbh);

our $server = 'http://ws/api';

our $authInfo = {
    'path' => '/authentication/v1',
    'username' => 'FINANCE_WS_USER',
    'password' => 'invoice',
};

our $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;

1;