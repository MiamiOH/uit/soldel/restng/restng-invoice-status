#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {
    $dbh->do(qq{ delete from fzbsqex });
    $dbh->do(qq{ delete from error_log_web_service });

    $dbh->do(q{
        insert into fzbsqex (fzbsqex_id, fzbsqex_invoice_number, fzbsqex_status, fzbsqex_payment_method, fzbsqex_record_number, fzbsqex_record_date, fzbsqex_notes)
            values (?, ?, ?, ?, ?, ?, ?)
        }, undef, '1', 'W0207510', 'payable', 'ACH', '1234', '2015-04-29', 'test note');
        
    $dbh->do(q{
        insert into fzbsqex (fzbsqex_id, fzbsqex_invoice_number, fzbsqex_status, fzbsqex_payment_method, fzbsqex_record_number, fzbsqex_record_date, fzbsqex_notes)
            values (?, ?, ?, ?, ?, ?, ?)
        }, undef, '2', 'W0207511', 'cancelled', '', '', '', '');

};