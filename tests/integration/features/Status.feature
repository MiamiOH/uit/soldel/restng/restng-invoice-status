# Test usage of the invoice status service
Feature: Get a list of invoice status updates that need exported to an external site
 As a consumer of the invoice status service
 I want to get a list of invoice status updates
 In order to export them to an external web service

Background:
  Given the test data is ready

Scenario: Require authentication to access the get resource
  Given a REST client
  When I make a GET request for /finance/accountsPayable/v1/invoice/status
  Then the HTTP status code is 401

Scenario: Get an invoice status list
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /finance/accountsPayable/v1/invoice/status
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a recordId key equal to "1"
  And the response data element first entry contains a invoiceNumber key equal to "W0207510"
  And the response data element first entry contains a invoiceStatus key equal to "payable"
  And the response data element first entry contains a paymentMethod key equal to "ACH"
  And the response data element first entry contains a recordNumber key equal to "1234"
  And the response data element first entry contains a recordDate key equal to "2015-04-29"
  And the response data element first entry contains a notes key equal to "test note"
  And the response data element second entry contains a recordId key equal to "2"
  And the response data element second entry contains a invoiceNumber key equal to "W0207511"
  And the response data element second entry contains a invoiceStatus key equal to "cancelled"
  And the response data element second entry contains a paymentMethod key that is empty
  And the response data element second entry contains a recordNumber key that is empty
  And the response data element second entry contains a recordDate key that is empty
  And the response data element second entry contains a notes key that is empty
  
Scenario: Require authentication to access the delete resource
  Given a REST client
  When I make a DELETE request for /finance/accountsPayable/v1/invoice/status/1
  Then the HTTP status code is 401
  
Scenario: Delete an invoice status record
  Given a REST client
  And a valid token for an authorized user
  And that a fzbsqex record with fzbsqex_id 1 does exist
  When I make a DELETE request for /finance/accountsPayable/v1/invoice/status/1
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a fzbsqex record with fzbsqex_id 1 does not exist
  
Scenario: Require authentication to access the log an error
  Given a REST client
  When I make a PUT request for /finance/accountsPayable/v1/invoice/status/log/error
  Then the HTTP status code is 401
  
Scenario: Log an error
  Given a REST client
  And a valid token for an authorized user
  And that a error_log_web_service record with fzbsqex_id 1 does not exist
  When I have a message with the recordId "1"
  And I have a message with the errorMessage "there was an error!!!"
  And I have a message with the invoiceNumber "W0207511"
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /finance/accountsPayable/v1/invoice/status/log/error
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And a error_log_web_service record with fzbsqex_id 1 does exist