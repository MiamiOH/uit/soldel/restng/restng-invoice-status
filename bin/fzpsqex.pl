#!/usr/bin/perl
#----------------------------------------------------------------------
#FILE NAME:  fzpsqex.pl
#
#Copyright (c) 2015 Miami University, All Rights Reserved.
#
#AUTHOR: Amy Tackett
#
#DESCRIPTION: Posts invoice status changes to SciQuest web service 
# 
#
#TABLES/USAGE: 
#	fzbsqex - SELECT, UPDATE
#                         
#AUDIT TRAIL:
#DATE        PRJ-TSK          UNIQUEID         VERSION
#4/2015      created          tacketal
#Description:  Created as part of SciQuest AP Directory project
#------------------------------------------------------------------------------

use lib '$ENV{BANNER_HOME}/general/perl';
use strict;
use warnings; 

use Getopt::Long;
use LWP::UserAgent;
use JSON;
use XML::Writer;
use XML::Simple;
use Data::Dumper;

our $logFile = '';
my $configFile = '';

my $result = GetOptions('log=s' => \$logFile, 'config=s' => \$configFile);

unless ($configFile) {
  print "Please provide a valid config file\n";
  exit 1;
}

our %config;
open CONFIG, $configFile or die "Couldn't open $configFile: $!";
while (<CONFIG>) {
  chomp;
  next unless ($_);
  my ($key, $value) = split(/\s*=\s*/, $_, 2);
  $config{$key} = $value;
}
close CONFIG;

$logFile = $config{'logFile'} unless ($logFile);
open LOG, ">$logFile" or die "Couldn't open $logFile: $!";
close LOG;

my $miamiHost = $config{'miamiHost'};
my $miamiUsername = $config{'miamiUsername'};
my $miamiPassword = $config{'miamiPassword'};
my $sciquestHost = $config{'sciquestHost'};
my $sciquestUsername = $config{'sciquestUsername'};
my $sciquestPassword = $config{'sciquestPassword'};

my $authWSEndpoint = $miamiHost . '/authentication/v1';
my $statusWSEndpoint = $miamiHost . '/finance/accountsPayable/v1/invoice/status';
my $sciquestEndpoint = $sciquestHost . '/apps/Router/InvoiceStatusMessage';

my $ua = LWP::UserAgent->new;
 
my $getAuthReq = HTTP::Request->new(POST => $authWSEndpoint);
$getAuthReq->header('content-type' => 'application/json');
$getAuthReq->content('{"type":"usernamePassword", "username":"' . $miamiUsername . '","password":"' . $miamiPassword . '"}');
my $getAuthResp = $ua->request($getAuthReq);
if ($getAuthResp->is_success) {
	my $authJson = $getAuthResp->decoded_content;
    my $authRecord = JSON->new->utf8->decode($authJson);
    my $token = $authRecord->{data}->{token};

	my $getStatusReq = HTTP::Request->new(GET => $statusWSEndpoint . '?token=' . $token);
	$getStatusReq->header('content-type' => 'application/json');
	my $getStatusResp = $ua->request($getStatusReq);
	if ($getStatusResp->is_success) {
    	my $statusJson = $getStatusResp->decoded_content;
    	my $statusRecords = JSON->new->utf8->decode($statusJson);
		for my $record( @{$statusRecords->{data}} ){
			my $sciquestXML = '';
			my $writer = XML::Writer->new(
				OUTPUT=> \$sciquestXML,
				ENCODING => 'utf8',
			);
	
			$writer->xmlDecl('UTF-8');
			$writer->doctype('InvoiceStatusMessage', undef, $sciquestHost . '/app_docs/dtd/invoice/InvoiceStatus.dtd');
			$writer->startTag('InvoiceStatusMessage', version => '1.0');
  				$writer->startTag('Header');
    				#$writer->dataElement(MessageId => '1');
    					$writer->startTag('Authentication');
      						$writer->dataElement(Identity => $sciquestUsername);
      							$writer->dataElement(SharedSecret => $sciquestPassword);
    					$writer->endTag('Authentication');
  				$writer->endTag('Header');
  				$writer->startTag('InvoiceStatuses');

					$writer->startTag('Invoice');
      					$writer->dataElement(InvoiceNumber => $record->{invoiceNumber});
      					$writer->dataElement(Status => $record->{invoiceStatus});
      					$writer->dataElement(PaymentMethod => $record->{paymentMethod});
      					$writer->dataElement(RecordNumber => $record->{recordNumber});
      					$writer->dataElement(RecordDate => $record->{recordDate});
      					$writer->dataElement(Notes => $record->{notes});
    				$writer->endTag('Invoice');

				$writer->endTag('InvoiceStatuses');
			$writer->endTag('InvoiceStatusMessage');

			my $sciquestReq = HTTP::Request->new(POST => $sciquestEndpoint);
			$sciquestReq->header('content-type' => 'text/xml');
 
			$sciquestReq->content($sciquestXML);
 
			my $sciquestResp = $ua->request($sciquestReq);
			if ($sciquestResp->is_success) {
				my $statusResults = XMLin($sciquestResp->decoded_content);
				if ($statusResults->{ResponseMessage}->{ResponseStatus}->{StatusCode} eq '200') {
					print "Record successfully updated with SciQuest: " . $record->{recordId} . "\n";
					
					my $deleteStatusReq = HTTP::Request->new(DELETE => $statusWSEndpoint . '/' . $record->{recordId} . '?token=' . $token);
					$deleteStatusReq->header('content-type' => 'application/json');

					my $deleteStatusResp = $ua->request($deleteStatusReq);
					if ($deleteStatusResp->is_success) {
     					print "Record successfully deleted: " . $record->{recordId} . "\n";
					}
					else {
    					print "InvoiceStatus DELETE error: " . $getAuthResp->code . " " . $getAuthResp->message . "ID " . $record->{recordId} . "\n";
						logEntry('message' => 'InvoiceStatus DELETE: ' . $getAuthResp->code . ' ' . $getAuthResp->message . 'ID ' . $record->{recordId});
					}
				}
				else {
					print "Record failed on SciQuest update - record ID: " . $record->{recordId} . "\n";
					logEntry('message' => 'Record failed on SciQuest update - record ID: ' . $record->{recordId});
#					my $recordId = $record->{recordId};
#					my $invoiceNumber = $record->{invoiceNumber};
#					my $errorMessage = $statusResults->{ResponseMessage}->{InvoiceErrors}->{InvoiceError}->{Errors}->{Error}->{ErrorMessage};
#					my $logStatusErrorReq = HTTP::Request->new(PUT => $statusWSEndpoint . '/log/error?token=' . $token);
#					$logStatusErrorReq->header('content-type' => 'application/json');
#					$logStatusErrorReq->content('{"recordId":"' . $recordId . '", "invoiceNumber":"' . $invoiceNumber . '","errorMessage":"' . $errorMessage . '"}');
#
#					my $logStatusErrorResp = $ua->request($logStatusErrorReq);
#					if ($logStatusErrorResp->is_success) {
#	   					my $message = $logStatusErrorResp->decoded_content;
#    					print "Received reply from log error request:" . $message . "\n";
#					}
#					else {
#    					print "HTTP PUT error code: ", $logStatusErrorResp->code, "\n";
#    					print "HTTP PUT error message: ", $logStatusErrorResp->message, "\n";
#					}
				}
			}
			else {
				print "SciQuest error: " . $getAuthResp->code . " " . $getAuthResp->message . "\n";
				logEntry('message' => 'SciQuest error: ' . $getAuthResp->code . ' ' . $getAuthResp->message);
			}
		}
	}
	else {
    	print "InvoiceStatusWS error: " . $getAuthResp->code . " " . $getAuthResp->message . "\n";
    	logEntry('message' => 'InvoiceStatusWS error: ' . $getAuthResp->code . ' ' . $getAuthResp->message);
	}
}
else {
    print "AuthWS error: " . $getAuthResp->code . " " . $getAuthResp->message . "\n";
    logEntry('message' => 'AuthWS error: ' . $getAuthResp->code . ' ' . $getAuthResp->message);
}

sub logEntry {
  my $params = { @_ };

  my $message = $params->{'message'} || '';

  if ($message) {
    my $time = scalar localtime();
    #print "[$time] $message\n";
    open LOG, ">>$logFile" or die "Couldn't open $logFile: $!";
    print LOG "[$time] $message\n";
    close LOG;
  }
}