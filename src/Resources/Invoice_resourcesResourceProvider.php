<?php

namespace MiamiOH\ProjectsInvoicestatus\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class Invoice_resourcesResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

            }

    public function registerServices(): void
    {
                    $this->addService(array(
        'name' => 'InvoiceStatus',
        'class' => 'MiamiOH\ProjectsInvoicestatus\Services\Status',
        'description' => 'Provide invoice status service.',
        'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
          ),
    ));

            }

    public function registerResources(): void
    {
                    $this->addResource(array(
    'action' => 'read',
    'name' => 'finance.accountsPayable.v1.invoice.status',
    'description' => 'Get a list of invoice statuses that need exported to external site.',
    'pattern' => '/finance/accountsPayable/v1/invoice/status',
    'service' => 'InvoiceStatus',
    'method' => 'getStatusList',
    'returnType' => 'collection',
    'middleware' => array(
            'authenticate' => array(),
            'authorize' => array(
                            'application' => 'Finance Web Services',
                            'module' => 'InvoiceStatus',
                            'key' => 'view'),
    ),
));

                    $this->addResource(array(
    'action' => 'delete',
    'name' => 'finance.accountsPayable.v1.invoice.status.id.delete',
    'description' => 'Delete an invoice status record.',
    'pattern' => '/finance/accountsPayable/v1/invoice/status/:id',
    'service' => 'InvoiceStatus',
    'method' => 'deleteStatus',
    'returnType' => 'model',
    'params' => array('id' => array('description' => 'ID for the invoice status record')),
    'middleware' => array(
            'authenticate' => array(),
            'authorize' => array(
                            'application' => 'Finance Web Services',
                            'module' => 'InvoiceStatus',
                            'key' => 'delete'),
    ),
));

                    $this->addResource(array(
    'action' => 'update',
    'name' => 'finance.accountsPayable.v1.invoice.status.error',
    'description' => 'Log an error message from failed attempt to POST to SciQuest.',
    'pattern' => '/finance/accountsPayable/v1/invoice/status/log/error',
    'service' => 'InvoiceStatus',
    'method' => 'logError',
    'returnType' => 'model',
    'middleware' => array(
            'authenticate' => array(),
            'authorize' => array(
                            'application' => 'Finance Web Services',
                            'module' => 'InvoiceStatus',
                            'key' => 'update'),
    ),
));

            }

    public function registerOrmConnections(): void
    {

    }
}