<?php

namespace MiamiOH\ProjectsInvoicestatus\Services;

class Status extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'MU_GAC';
    private $dbh;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function getStatusList()
    {
        $response = $this->getResponse();

        $statusList = $this->dbh->queryall_array(
            'select fzbsqex_id as record_id,
        	   fzbsqex_invoice_number as invoice_number,
               fzbsqex_status as invoice_status,
               fzbsqex_payment_method as payment_method,
               fzbsqex_record_number as record_number,
               fzbsqex_record_date as record_date,
               fzbsqex_notes as notes
        from fzbsqex
        order by fzbsqex_id'
        );

        for ($i = 0; $i < count($statusList); $i++) {
            $statusList[$i] = $this->camelCaseKeys($statusList[$i]);
        }

        $response->setPayload($statusList);

        return $response;
    }

    public function deleteStatus()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $recordId = $request->getResourceParam('id');

        $this->dbh->perform('
        delete from fzbsqex where fzbsqex_id = ?
      ', $recordId);

        return $response;
    }

    public function logError()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $data = $request->getData();

        $this->dbh->perform('
        insert into error_log_web_service 
          (error_date, error_text, invoice_number, fzbsqex_id)
        values
          (sysdate, ?, ?, ?)
      ', $data['errorMessage'], $data['invoiceNumber'], $data['recordId']);

        return $response;
    }

}